//
//  Alamofire.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import Foundation
import UIKit

struct CategoryID: Decodable {
    var id: Int
    var title: String
    
    init?(json: [String: Any]) {
        let id = json["qid"] as? Int
        let title = json["title"] as? String
        
        
        self.id = id ?? 0
        self.title = title ?? ""
        
    }
    
    static func getArray( from jsonArray: Any) -> [CategoryID]? {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else {return nil}
        return jsonArray.compactMap { CategoryID(json: $0) }
    }
}

struct Questions: Decodable {
    var questionID: String
    var questionCode: String
    var question: String
    var comment: String
    var categoryID: String
    var origin: String
    var categoryNewID: String
    
    init?(json: [String: Any]) {
        let questionID = json["questionid"] as? String
        let questionCode = json["questioncode"] as? String
        let question = json["question"] as? String
        let comment = json["comment"] as? String
        let categoryID = json["categoryid"] as? String
        let origin = json["origin"] as? String
        let categoryNewID = json["categorynewid"] as? String
        
        
        self.questionID = questionID ?? ""
        self.questionCode = questionCode ?? ""
        self.question = question ?? ""
        self.comment = comment ?? ""
        self.categoryID = categoryID ?? ""
        self.origin = origin ?? ""
        self.categoryNewID = categoryNewID  ?? ""
    }
    
    static func getArray( from jsonArray: Any) -> [Questions]? {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else {return nil}
        return jsonArray.compactMap { Questions(json: $0) }
    }
}







