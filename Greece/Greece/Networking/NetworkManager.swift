//
//  NetworkManager.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import Foundation
import UIKit
import Alamofire

final class NetworkManager {
    static let shared = NetworkManager()
}

extension NetworkManager {
    func getCategory(completion: @escaping ([CategoryID]) ->()) {
        AF.request("http://dev.eraappmobile.com/api/question", method: .get).responseJSON { result in
            switch result.result {
            case .success(let value):
                var data: [CategoryID] = []
                data = CategoryID.getArray(from: value) ?? []
                completion(data)
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    func getQuestions(id: Int, completion: @escaping ([Questions]) ->()) {
 
        let docParams: [String: Any] = [
            "qid": id
        ]
        AF.request("http://dev.eraappmobile.com/api/question", method: .post, parameters: docParams).responseJSON { result in
            switch result.result {
            case .success(let value):
                var data: [Questions] = []
                data = Questions.getArray(from: value) ?? []
                
                completion(data)
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
 
    
   
 
    
    
}
