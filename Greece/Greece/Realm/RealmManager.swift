//
//  RealmManager.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    let realm = try? Realm()
    
    private init() {}
    
    func readObject() -> [SavedQuestions]? {
        let objects = try? Realm().objects(SavedQuestions.self).toArray(ofType: SavedQuestions.self) as [SavedQuestions]
        
        return objects?.count ?? 0 > 0 ? objects : nil
    }
    
    
    func updateValue(question: SavedQuestions) {
        try? realm?.write {
            
        }
    }
    
    
    func writeQuestions(question: SavedQuestions) {
        try? realm?.write {
            realm?.add(question)
            print(question.questionCode)
        }
    }
    
    func deleteAll() {
        try? realm?.write {
            realm?.deleteAll()
        }
    }
    
}

class AnswerManager {
    static let shared = AnswerManager()
    let realm = try? Realm()
    
    private init() {}
    
    func readObject() -> [SavedAnswer]? {
        let objects = try? Realm().objects(SavedAnswer.self).toArray(ofType: SavedAnswer.self) as [SavedAnswer]
        
        return objects?.count ?? 0 > 0 ? objects : nil
    }
    
    
    func writeAnswer(answer: SavedAnswer) {
        try? realm?.write {
            realm?.add(answer)
            print(answer.questionID)
        }
    }
    
    func deleteAnswer(answer: SavedAnswer) {
        try? realm?.write {
            realm?.delete(answer)
        }
    }
    
    func deleteAll() {
        try? realm?.write {
            realm?.deleteAll()
        }
    }
    
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }

        return array
    }
}





