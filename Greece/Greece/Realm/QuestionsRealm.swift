//
//  QuestionsRealm.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import Foundation
import RealmSwift


class SavedQuestions: Object {
    @objc dynamic var questionID = ""
    @objc dynamic var questionCode = ""
    @objc dynamic var question = ""
    @objc dynamic var comment = ""
    @objc dynamic var categoryID = ""
    @objc dynamic var origin = ""
    @objc dynamic var categoryNewID = ""
    @objc dynamic var isAnswered = false
    
    
    convenience init(questionID: String, questionCode: String, question: String, comment: String, categoryID: String, origin: String, categoryNewID: String, isAnswered: Bool) {
        self.init()
        self.questionID = questionID
        self.question = question
        self.comment = comment
        self.categoryID = categoryID
        self.origin = origin
        self.categoryNewID  = categoryNewID
        self.questionCode = questionCode
        self.isAnswered = isAnswered
    }
}

class SavedAnswer: Object {
    @objc dynamic var inspectorName = ""
    @objc dynamic var answer = 0
    @objc dynamic var comment = ""
    @objc dynamic var dateOfCreation = ""
    @objc dynamic var questionID = ""
    @objc dynamic var questionCode = ""
    @objc dynamic var origin = ""
    @objc dynamic var categoryNewID = ""
    @objc dynamic var categoryID = ""
    
    convenience init(inspectorName: String, answer: Int, comment: String, questionID: String, origin: String, categoryNewID: String, dateOfCreation: String, questionCode: String, categoryID: String) {
        self.init()
        self.inspectorName = inspectorName
        self.answer = answer
        self.questionID = questionID
        self.origin = origin
        self.categoryNewID  = categoryNewID
        self.dateOfCreation = dateOfCreation
        self.questionCode = questionCode
        self.categoryID = categoryID
    }
}
