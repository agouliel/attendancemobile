//
//  CompletedQuestions.swift
//  Greece
//
//  Created by SSR Lab on 20.02.22.
//

import Foundation

class CompletedQuestions {
   static let shared = CompletedQuestions()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    

    var array: [String] {
        set {
            defaults.set(newValue, forKey: "completedQuestions")
        }
        get {
             return defaults.value(forKey: "completedQuestions") as? [String] ?? []
        }
    }
    


}
