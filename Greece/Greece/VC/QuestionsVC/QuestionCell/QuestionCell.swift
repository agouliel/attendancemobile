//
//  QuestionCell.swift
//  Greece
//
//  Created by Ilya Rabyko on 15.02.22.
//

import UIKit

class QuestionCell: UITableViewCell {
    @IBOutlet weak var buttonsView: UIView!
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        buttonsView.layer.cornerRadius = 15
        buttonsView.layer.borderColor = UIColor.black.cgColor
        buttonsView.layer.borderWidth = 1
    }
    
    
    
    
    
}
