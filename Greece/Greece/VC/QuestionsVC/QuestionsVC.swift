//
//  QuestionsVC.swift
//  Greece
//
//  Created by Ilya Rabyko on 15.02.22.
//

import UIKit
import RealmSwift
import Alamofire

class QuestionsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var questions: [SavedQuestions] = []
    var allQuestion: [SavedQuestions] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        
        setupController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
   // International Code for Fire Safety Systems (FSS Code)  {IMO 2nd 2007}
    func setupController() {
     //   Are the engine room bilge oily water pumping and disposal arrangements in good order?
//        self.questions = RealmManager.shared.readObject() ?? []
        
//        if CompletedQuestions.shared.array.count == 0 {
//            print("")
//        } else {
//            for question in allQuestion {
//                print(question.questionID)
//                for questionID in CompletedQuestions.shared.array {
//                             if questionID == question.questionID {
//                                 print("Question was removed: \(question.question)")
//                             } else {
//                                 self.questions.append(question)
//                                 print(question.questionID)
//                             }
//                         }
//            }
//        }
      
            
//
        
//        for question in questions {
//            print("Question_code: \(question.questionCode)")
//            id += 1
//            for questionID in CompletedQuestions.shared.array {
//                if questionID == question.questionID {
//                    questions.remove(at: id - 1)
//                    tableView.reloadData()
//                }
//            }
//        }
        
        self.navigationController?.navigationBar.isHidden = true
        tableView.setupDelegateData(self)
        tableView.registerCell(QuestionCell.self)
        self.updateTableView()
    }
    //Are there adequate procedures to prevent uncontrolled entry into the engine room?
    
    func updateTableView() {
        self.questions = []
        guard let currentQuestion = RealmManager.shared.readObject()?.filter({$0.isAnswered == false}) else { return }
        
        self.questions = currentQuestion
        tableView.reloadData()
    }
    func openAlert(question: String, arrayTag: String, answer: String) {
        let alertController = UIAlertController(title: "Are you sure of the answer?\nYour answer is: \(answer)", message: "\(question)", preferredStyle: UIAlertController.Style.actionSheet)
        
       
        alertController.addAction(UIAlertAction(title: "Save answer", style: .default, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            guard let tag = Int(arrayTag) else { return }
            guard let question = self?.questions[tag] else { return }
            var answerTag: Int!
            if answer == "Yes" {
                answerTag = 2
            } else {
                answerTag = 3
            }
            AnswerManager.shared.writeAnswer(answer: SavedAnswer(inspectorName: "test", answer: answerTag, comment: "", questionID: question.questionID, origin: question.origin, categoryNewID: question.categoryNewID, dateOfCreation:  MainViewController.dateFormatter.string(from: Date()), questionCode: question.questionCode, categoryID: question.categoryID))
            if Reachability.isConnectedToNetwork() {
                self?.sendAnswer()
            } else {
                print("No Connection")
            }
//            CompletedQuestions.shared.array.append(question.questionID)
            let currentQuestion = RealmManager.shared.readObject()?.filter({$0.questionID == question.questionID})
            let realm = try! Realm()
            if let currentQuestion = currentQuestion?.first {
                try! realm.write {
                    currentQuestion.isAnswered = true
                }
            }
            sSelf.updateTableView()
            print("saved to \(tag), text: \(question)")
        }))
        
        alertController.addAction(UIAlertAction(title: "Change answer", style: .destructive, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            print(sSelf)
            print("Change answer")
        }))
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            sSelf.present(alertController, animated: true, completion:{})
        }
    }
    
    func sendAnswer() {
        guard let array = AnswerManager.shared.readObject() else { return }
        var index = 0
        for answer in array {
            index += 1
            guard let categoryID = Int(answer.categoryID) else { return }
            let docParams: [String: Any] = [
                "answer": [
                    "InspectorName": answer.inspectorName,
                    "answer": answer.answer,
                    "comment": "comment_test",
                    "date_of_creation": answer.dateOfCreation,
                    "questionid": answer.questionID,
                    "questioncode": answer.questionCode,
                    "categoryid": categoryID,
                    "origin": answer.origin,
                    "categorynewid": answer.categoryNewID
                ]
            ]
            
            AF.request("http://dev.eraappmobile.com/api/answer", method: .post, parameters: docParams, encoding: JSONEncoding.default).responseString { result in
                switch result.result {
                case .success(let value):
                    AnswerManager.shared.deleteAnswer(answer: answer)
                    print(value)
                case .failure(let error):
                    print(error)
                }
                
            }
            
            print("success")
            
            
        }
        
    }
    
//    func sendAnswer(id: String, question: SavedQuestions, answerTag: Int ) {
//        if Reachability.isConnectedToNetwork() {
//        guard let answer = AnswerManager.shared.readObject()?.filter({$0.questionID == id}) else { return }
//        if let currentAnswer = answer.first {
//            guard let categoryID = Int(currentAnswer.categoryID) else { return }
//            let docParams: [String: Any] = [
//                "answer": [
//                    "InspectorName": currentAnswer.inspectorName,
//                    "answer": currentAnswer.answer,
//                    "comment": "comment_test",
//                    "date_of_creation": currentAnswer.dateOfCreation,
//                    "questionid": currentAnswer.questionID,
//                    "questioncode": currentAnswer.questionCode,
//                    "categoryid": categoryID,
//                    "origin": currentAnswer.origin,
//                    "categorynewid": currentAnswer.categoryNewID
//                ]
//            ]
//            AF.request("http://dev.eraappmobile.com/api/answer", method: .post, parameters: docParams, encoding: JSONEncoding.default).responseString { result in
//                switch result.result {
//                case .success(let value):
//                    AnswerManager.shared.deleteAnswer(answer: currentAnswer)
//                    print(value)
//                case .failure(let error):
//                    print(error)
//                }
//            }
//        }
//
//        } else {
//            AnswerManager.shared.writeAnswer(answer: SavedAnswer(inspectorName: "test", answer: answerTag, comment: "", questionID: question.questionID, origin: question.origin, categoryNewID: question.categoryNewID, dateOfCreation:  MainViewController.dateFormatter.string(from: Date()), questionCode: question.questionCode, categoryID: question.categoryID))
//        }
//            print("success")
//
//
//
//
//    }
    
    
    @objc func saveNoAnswer(sender: UIButton) {
        openAlert(question: "\(questions[sender.tag].question)", arrayTag: "\(sender.tag)", answer: "no")
    }
    
    @objc func saveYesAnswer(sender: UIButton) {
        openAlert(question: "\(questions[sender.tag].question)", arrayTag: "\(sender.tag)", answer: "Yes")
    }
    
    @objc func openInfo(sender: UIButton) {
        let storboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storboard.instantiateViewController(withIdentifier: "InfoVC") as? InfoVC else { return }
        vc.comments = questions[sender.tag].comment
        navigationController?.present(vc, animated: true)
    }
    
    
    
}


extension QuestionsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: QuestionCell.self), for: indexPath)
        guard  let questionCell = cell as? QuestionCell else {return cell}
        if questions[indexPath.row].comment == "" {
            questionCell.infoButton.isHidden = true
        } else {
            questionCell.infoButton.isHidden = false
        }
        questionCell.yesButton.tag = indexPath.row
        questionCell.noButton.tag = indexPath.row
        questionCell.infoButton.tag = indexPath.row
        questionCell.noButton.addTarget(self, action: #selector(saveNoAnswer(sender:)), for: .touchUpInside)
        questionCell.yesButton.addTarget(self, action: #selector(saveYesAnswer(sender:)), for: .touchUpInside)
        questionCell.infoButton.addTarget(self, action: #selector(openInfo(sender:)), for: .touchUpInside)
        questionCell.questionLabel.text = questions[indexPath.row].question
        return questionCell
    }
}

