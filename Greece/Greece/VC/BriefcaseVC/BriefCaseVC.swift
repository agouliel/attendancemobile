//
//  BriefCaseVC.swift
//  Greece
//
//  Created by SSR Lab on 23.02.22.
//

import UIKit



enum contentType {
    case vessel
    case port
    case inspectionType
    case inspector
    case questionnaires
}

class BriefCaseVC: UIViewController {

    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var vessels: [String] = ["DROMEAS", "ESTIA", "EVRIDIKI", "FIDIAS" , "GEA" , "KOUROS" , "KRITON", "LYSIAS", "ORFEAS", "PLOUTOS" , "THEO T"]
    var inspectionType: [String] = ["Audit", "Vetting", "Attendance", "Dry Dock", "Site Visits", "Terminal Inspection", "Third Party Inspection", "STAR TANKER Inspection", "MultiType Inspection"]
    
    var dubPorts: [String] = ["AT SEA","Jebel Ali","Le Havre","Oita","Venice","Shenzhen","Huangpu","Mailiao","Burgas","Thessaloniki","Melaka","Agioi Theodoroi","Cartagena","Mailao","From Malta to Cartagena","Antwerp","S. Panagia","Augusta","Fujairah","Ghent","Tangjung Lasat","Rotterdam","Chiba","Singapore","Spore","Aghioi Theodoroi","Huelva","Lagos","Malta","Kostanza","Incheon","Yosu","Off Southwold","Lavera","Ulsan","Alexandria","Sarroch","Mailia","Ceyhan","Lagos (anchorage)","Huizhu","Daesan","Costantza","KOUROS","Singapore, AT SEA","Novorossiysk","Sao Sebastiao","Qingdao","Bahrain","Malta (OPL)","Skikda","From Piraeus To Malta","Off Malta","Koper","Long Beach -Kalifornia","Mersin","Antalya","Prindisi","Freeport","Cotonou","Suez","Sete","Savannah","JEBEL ALI","Constanta","Constantza","Elefsina","Fiumicino","Barcelona","Piraeus-Omisalj-Venica","Agioi Theodoroi,Bourgas","La Pallice","Sohar to Fujairah","Lingue","KOPPER - SUEZ","Cotonou-Lagos","AT SEA FROM GIBRALTAR TO NEW YORK","Augusta, Italy","Sakai, Japan","Thessaloniki, Greece","Jacksonville, FL","PALDISKI, ESTONIA","COTONOU","VALENCIA","Augusta - Italy","Ashkelon, Israel","Aliaga","Cyprus, offshore, STS","Port Arthur, Texas","Yeosu","Santa Panagia, Italy","Richmond, CA USA","Jebel Ali, Tanker Berth #1, UAE","Civitavecchia, Italy","Lagos, Anchorage.","AT SEA FRO SINGAPORE TO YEOSU","AT SEA FROM FUJAIRAH TO KUWAIT","SUEZ - ROTTERDAM","Askelon","Bin Qasim","Thessaloniki to Malta OPL","OPL MALTA","Tarragona","Ag. Theodoroi","Dickson","Bayonne, NJ, USA","Lome / Togo","Piraeus","Antwerp to Baton Rouge","JEBEL ALI TO KARACHI","Cotonou  to Lome","Lome  to P. Harcourt","Augusta - Tunnis","ANTWEP","Ashkelon - Naples","OPL Malta to Venice","MILLAZO","Las Palmas to Rotterdam","Zarate","LAGOS","Gibraltar","Abidjan","Aden","Santa Panagia","Port Qasim","Sikka","Bilbao","Karachi","Port Sikka","Vopac Banyan Jetty #5","Baton Rouge, TX","New Orleans to Baton Rouge LA","Chiba / Idemitsu No. 1 pier","Ras Tanura","Europort","Porto Marghera","Iskenderun","Jorf Lasfar","Ashkelon","Laskhra","From Ventspls to Skagen","From: Singapore To: Kaohsiung, Taiwan","Fm: Pascagoula, U.S To: Freeport, Bahamas","MALTA","Brindisi","La Skhira","Malta-Brindisi","From Malta to La Shkira","AMA anchorage, Louisiana","LAGOS ANCHORAGE","Tutunciftlik","Sea","Limassol","NEW YORK","IMTT-Bayonne, New Jersey","St.Panagia","Houston","Primorsk","Fawley","Amsterdam","Thames Haven","Teesport","At Lagos","From: Ventspils To: Amsterdam","From: Riga To: Teesport,Uk","at Thames Haven, UK","Ventspills","VENTSPILS","Braintree, Massachusetts","St. Panagia","Port Everglades","Slagen","Galveston","Houston, Texas","Jamnagar terminal, Sikka","LAS PALMAS","Bizerte","Tanjung Pelepas","Halifax","Ereglisi","KARACHI","TANJUNG PELEPAS","St Charles","Tunis","STS off Lome","Marmara Ereglisi","CORPUS CHRISTI","Pajaritos","Aruba Anchorage","Pasir Gudang","Algeciras","Long Beach, LA","Aruba","Long Beach","KOPER","MAILIAO","Lome","SLAGEN","New York","SARROCH","Anyer","LA","AMS TO LASPALMAS","Chittagong","ADABIYA TO ASPROPYRGOS","Reading, NJ","Baytown","El Segundo","Boston","Ventspils","Providence","Corpus Christi, Texas","Dunkerque","Corpus Christi","Bourgas","P Marghera","Lake Charles","DAESAN","La Porte","Itaqui Terminal","Guayanilla","Houizhou","Huizhou","Tuapse","Taipei","Taichung","Mombasa","SUEZ-ALGECIRAS","New Orlean","New Orleans","Hamburg","Pembroke to Boston","Ennore","Algiers","Balboa to Everglades","OAKLAND","Richmond","Garyville","FOS","Singapore - Yeosu","Fos","Map Ta Phut","Genoa","Kandla","San Francisco","Port Aurthur","Los Angeles","Aqaba","Derince","Durban","Las Palmas","OPL Malta","Malta OPL","SINGAPORE_ULSAN","Philadelphia, PA","Cristobal to Come by chance","Immingham","Philadelphia","Qubec","Quebec","Kalundborg","Rosarito","Rosaritos","HUELVA","Carteret","Port Arthur","Tanjung Uban","OPL Limassol","PRIMORSK","Trieste","Cherry point Usa","Trabzon","Jose Terminal","Askhelon","Taranto","Kaohsiung","Civitavecchia","Maputo","Mundra","Balboa to San Francisco","Colombo","Saint Croix","Antwerpen","Balboa","El Dekheila","Vysotsk","AMSTERDAM","Afrika Haven","Port Of New York And New Jersey","Port Hedland","Hedland","Wilhelmshaven","Maliao","St.Croix","Fujeirah Anchorage","SUEZ","Salvador","Sika","Campana","Brisbane","Syros - Piraeus","Las Palmas - Mongstad","Terneuzen","Callao","Marsaxlokk","New Jersey","Goteborg","Skagen - Gothenburg","Melones Oil Terminal","Ciudad Madero","Las Palmas to Roterdam","GIBRALTAR OPL-ANTWERP","ROTTERDAM","Tracy","Dakar"
    ]
    var ports: [String] = ["AT SEA","Jebel Ali","LeHavre","Oita","Venice","Shenzhen","Huangpu","Mailiao","Burgas","Thessaloniki","Melaka","Agioi Theodoroi","Cartagena","Mailao","From Malta to Cartagena","Antwerp","S. Panagia","Augusta","Fujairah","Ghent","Tangjung Lasat","Rotterdam","Chiba","Singapore","Spore","Aghioi Theodoroi","Huelva","Lagos","Malta","Kostanza","Incheon","Yosu","Off Southwold","Lavera","Ulsan","Alexandria","Sarroch","Mailia","Ceyhan","Lagos (anchorage)","Huizhu","Daesan","Costantza","KOUROS","Singapore, AT SEA","Novorossiysk","Sao Sebastiao","Qingdao","Bahrain","Malta (OPL)","Skikda","From Piraeus To Malta","Off Malta","Koper","Long Beach -Kalifornia","Mersin","Antalya","Prindisi","Freeport","Cotonou","Suez","Sete","Savannah","JEBEL ALI","Constanta","Constantza","Elefsina","Fiumicino","Barcelona","Piraeus-Omisalj-Venica","Agioi Theodoroi,Bourgas","La Pallice","Sohar to Fujairah","Lingue","KOPPER - SUEZ","Cotonou-Lagos","AT SEA FROM GIBRALTAR TO NEW YORK","Augusta, Italy","Sakai, Japan","Thessaloniki, Greece","Jacksonville, FL","PALDISKI, ESTONIA","COTONOU","VALENCIA","Augusta - Italy","Ashkelon, Israel","Aliaga","Cyprus, offshore, STS","Port Arthur, Texas","Yeosu","Santa Panagia, Italy","Richmond, CA USA","Jebel Ali, Tanker Berth #1, UAE","Civitavecchia, Italy","Lagos, Anchorage.","AT SEA FRO SINGAPORE TO YEOSU","AT SEA FROM FUJAIRAH TO KUWAIT","SUEZ - ROTTERDAM","Askelon","Bin Qasim","Thessaloniki to Malta OPL","OPL MALTA","Tarragona","Ag. Theodoroi","Dickson","Bayonne, NJ, USA","Lome / Togo","Piraeus","Antwerp to Baton Rouge","JEBEL ALI TO KARACHI","Cotonou  to Lome","Lome  to P. Harcourt","Augusta - Tunnis","ANTWEP","Ashkelon - Naples","OPL Malta to Venice","MILLAZO","Las Palmas to Rotterdam","Zarate","LAGOS","Gibraltar","Abidjan","Aden","Santa Panagia","Port Qasim","Sikka","Bilbao","Karachi","Port Sikka","Vopac Banyan Jetty #5","Baton Rouge, TX","New Orleans to Baton Rouge LA","Chiba / Idemitsu No. 1 pier","Ras Tanura","Europort","Porto Marghera","Iskenderun","Jorf Lasfar","Ashkelon","Laskhra","From Ventspls to Skagen","From: Singapore To: Kaohsiung, Taiwan","Fm: Pascagoula, U.S To: Freeport, Bahamas","MALTA","Brindisi","La Skhira","Malta-Brindisi","From Malta to La Shkira","AMA anchorage, Louisiana","LAGOS ANCHORAGE","Tutunciftlik","Sea","Limassol","NEW YORK","IMTT-Bayonne, New Jersey","St.Panagia","Houston","Primorsk","Fawley","Amsterdam","Thames Haven","Teesport","At Lagos","From: Ventspils To: Amsterdam","From: Riga To: Teesport,Uk","at Thames Haven, UK","Ventspills","VENTSPILS","Braintree, Massachusetts","St. Panagia","Port Everglades","Slagen","Galveston","Houston, Texas","Jamnagar terminal, Sikka","LAS PALMAS","Bizerte","Tanjung Pelepas","Halifax","Ereglisi","KARACHI","TANJUNG PELEPAS","St Charles","Tunis","STS off Lome","Marmara Ereglisi","CORPUS CHRISTI","Pajaritos","Aruba Anchorage","Pasir Gudang","Algeciras","Long Beach, LA","Aruba","Long Beach","KOPER","MAILIAO","Lome","SLAGEN","New York","SARROCH","Anyer","LA","AMS TO LASPALMAS","Chittagong","ADABIYA TO ASPROPYRGOS","Reading, NJ","Baytown","El Segundo","Boston","Ventspils","Providence","Corpus Christi, Texas","Dunkerque","Corpus Christi","Bourgas","P Marghera","Lake Charles","DAESAN","La Porte","Itaqui Terminal","Guayanilla","Houizhou","Huizhou","Tuapse","Taipei","Taichung","Mombasa","SUEZ-ALGECIRAS","New Orlean","New Orleans","Hamburg","Pembroke to Boston","Ennore","Algiers","Balboa to Everglades","OAKLAND","Richmond","Garyville","FOS","Singapore - Yeosu","Fos","Map Ta Phut","Genoa","Kandla","San Francisco","Port Aurthur","Los Angeles","Aqaba","Derince","Durban","Las Palmas","OPL Malta","Malta OPL","SINGAPORE_ULSAN","Philadelphia, PA","Cristobal to Come by chance","Immingham","Philadelphia","Qubec","Quebec","Kalundborg","Rosarito","Rosaritos","HUELVA","Carteret","Port Arthur","Tanjung Uban","OPL Limassol","PRIMORSK","Trieste","Cherry point Usa","Trabzon","Jose Terminal","Askhelon","Taranto","Kaohsiung","Civitavecchia","Maputo","Mundra","Balboa to San Francisco","Colombo","Saint Croix","Antwerpen","Balboa","El Dekheila","Vysotsk","AMSTERDAM","Afrika Haven","Port Of New York And New Jersey","Port Hedland","Hedland","Wilhelmshaven","Maliao","St.Croix","Fujeirah Anchorage","SUEZ","Salvador","Sika","Campana","Brisbane","Syros - Piraeus","Las Palmas - Mongstad","Terneuzen","Callao","Marsaxlokk","New Jersey","Goteborg","Skagen - Gothenburg","Melones Oil Terminal","Ciudad Madero","Las Palmas to Roterdam","GIBRALTAR OPL-ANTWERP","ROTTERDAM","Tracy","Dakar"
    ]

   
    var contentType: contentType = .vessel
    override func viewDidLoad() {
        super.viewDidLoad()
     
    setupController()
       
    }
    
    func setupController() {
        search.delegate = self
        self.hideKeyboardWhenTappedAround()
        switch contentType {
        case .vessel:
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Vessels"
        case .port:
            searchHeight.constant = 50
            search.isHidden = false
            self.title = "Port"
        case .inspectionType:
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Inspection Type"
        case .inspector:
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Inspector"
        case .questionnaires:
            searchHeight.constant = 0 
            search.isHidden = true
            self.title = "Questionnaires"
        }
        collectionView.register(UINib(nibName: "BriefcaseCell", bundle: nil), forCellWithReuseIdentifier: "BriefcaseCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func openAlert(choice: String) {
        var alertController = UIAlertController()
        switch contentType {
        case .vessel:
            alertController = UIAlertController(title: "Vessel", message: "Are you sure that your vessel is \(choice)?", preferredStyle: .actionSheet)
        case .port:
            alertController = UIAlertController(title: "Port", message: "Are you sure that your port is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Port"
        case .inspectionType:
            alertController = UIAlertController(title: "Inspection Type", message: "Are you sure that your inspection type is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Inspection Type"
        case .inspector:
            alertController = UIAlertController(title: "Inspector", message: "Are you sure that your inspector is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Inspector"
        case .questionnaires:
            alertController = UIAlertController(title: "Questionnaire", message: "Are you sure that your Questionnaire is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Questionnaires"
        }
        
        alertController.addAction(UIAlertAction(title: "Continue", style: .default, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            switch sSelf.contentType {
            case .vessel:
                UserDefaults.standard.set(choice, forKey: "vessel")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
                vc.contentType = .port
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .port:
                UserDefaults.standard.set(choice, forKey: "port")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
                vc.contentType = .inspectionType
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .inspectionType:
                UserDefaults.standard.set(choice, forKey: "inspectionType")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
                vc.contentType = .inspector
                sSelf.navigationController?.pushViewController(vc, animated: true)            case .inspector:
              print("inspector")
            case .questionnaires:
                UserDefaults.standard.set(choice, forKey: "questionnaires")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
                vc.contentType = .questionnaires
                sSelf.navigationController?.pushViewController(vc, animated: true)
              print("questionnaires")
            }
            
        }))
        
 
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            print(sSelf)
            print("Cancel")
        }))
        
        navigationController?.present(alertController, animated: true)
//        let alertControllerdf = UIAlertController(title: "Are you sure of the answer?\nYour answer is: \(answer)", message: "\(question)", preferredStyle: UIAlertController.Style.actionSheet)
        
    }
    


}


extension BriefCaseVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            switch contentType {
            case .vessel:
                openAlert(choice: vessels[indexPath.row])
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
//                vc.contentType = .port
//                navigationController?.pushViewController(vc, animated: true)
            case .port:
                openAlert(choice: dubPorts[indexPath.row])
    
            case .inspectionType:
                openAlert(choice: inspectionType[indexPath.row])
            case .inspector:
              print("inspector")
            case .questionnaires:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BriefCaseVC") as! BriefCaseVC
                vc.contentType = .questionnaires
                navigationController?.pushViewController(vc, animated: true)
              print("questionnaires")
            }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch contentType {
        case .vessel:
            return vessels.count
        case .port:
            return dubPorts.sorted(by: {$0 > $1}).count
        case .inspectionType:
            return inspectionType.count
        case .inspector:
            return vessels.count
        case .questionnaires:
            return vessels.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BriefcaseCell", for: indexPath) as! BriefcaseCell
    
        switch self.contentType {
        case .vessel:
            cell.mainLabel.text = vessels[indexPath.row]
        case .port:
            cell.mainLabel.text = dubPorts[indexPath.row]
        case .inspectionType:
            cell.mainLabel.text = inspectionType[indexPath.row]
        case .inspector:
            cell.mainLabel.text = vessels[indexPath.row]
        case .questionnaires:
            cell.mainLabel.text = vessels[indexPath.row]
        }
        
        //    switch contentType {
        //    case .vessel:
        //      print("vessel")
        //    case .port:
        //      print("port")
        //    case .inspectionType:
        //      print("inspectionType")
        //    case .inspector:
        //      print("inspector")
        //    case .questionnaires:
        //      print("questionnaires")
        //
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size.width
        return CGSize(width: collectionViewSize, height: 65)
    }
    
    
}

extension BriefCaseVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search.setShowsCancelButton(false, animated: true)
        search.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dubPorts = []
        if searchText == "" {
            dubPorts = ports.sorted(by: {$0 > $1})
        }
        
        for item in ports {
            if item.uppercased().contains(searchText.uppercased()) {
                dubPorts.append(item)
            }
        }
        collectionView.reloadData()
    }
}

