//
//  BriefcaseCell.swift
//  Greece
//
//  Created by SSR Lab on 23.02.22.
//

import UIKit

class BriefcaseCell: UICollectionViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
