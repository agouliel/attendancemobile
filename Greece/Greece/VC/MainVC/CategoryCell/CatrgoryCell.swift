//
//  CatrgoryCell.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import UIKit

class CatrgoryCell: UITableViewCell {
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.borderColor = UIColor.black.cgColor
        self.mainView.layer.borderWidth = 1
    }
    
    
}
