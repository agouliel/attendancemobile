//
//  MainViewController.swift
//  Greece
//
//  Created by SSR Lab on 18.02.22.
//

import UIKit
import Alamofire

class MainViewController: UIViewController {
    public static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
        return formatter
    }()
    
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var categories: [CategoryID] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        sendAnswer()
        setupController()
    }
    
    func sendAnswer() {
        guard let array = AnswerManager.shared.readObject() else { return }
        var index = 0
        for answer in array {
            index += 1
            guard let categoryID = Int(answer.categoryID) else { return }
            let docParams: [String: Any] = [
                "answer": [
                    "InspectorName": answer.inspectorName,
                    "answer": answer.answer,
                    "comment": "comment_test",
                    "date_of_creation": answer.dateOfCreation,
                    "questionid": answer.questionID,
                    "questioncode": answer.questionCode,
                    "categoryid": categoryID,
                    "origin": answer.origin,
                    "categorynewid": answer.categoryNewID
                ]
            ]
            
            AF.request("http://dev.eraappmobile.com/api/answer", method: .post, parameters: docParams, encoding: JSONEncoding.default).responseString { result in
                switch result.result {
                case .success(let value):
                    AnswerManager.shared.deleteAnswer(answer: answer)
                    print(value)
                case .failure(let error):
                    print(error)
                }
                
            }
            
            print("success")
            
            
        }
        
    }
    
    func setupController() {
        tableView.setupDelegateData(self)
        tableView.registerCell(CatrgoryCell.self)
        alphaView.isHidden = true
        //MARK: Connection is available
        if Reachability.isConnectedToNetwork() {
            let array = RealmManager.shared.readObject()
            if array == nil {
                NetworkManager.shared.getCategory { [weak self] categories in
                    guard let sSelf = self else { return }
                    sSelf.categories = categories
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.tableView.reloadData()
                    }
                }
                // download array
            } else {
                let storboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storboard.instantiateViewController(withIdentifier: "QuestionsVC") as? QuestionsVC else { return }
                navigationController?.pushViewController(vc, animated: true)
                // send array to db
            }
            //MARK: Connection is not available
        } else {
            //open questions and save it to db
            let storboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storboard.instantiateViewController(withIdentifier: "QuestionsVC") as? QuestionsVC else { return }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func downdloadQuestions(category: CategoryID) {
        let alertController = UIAlertController(title: "Are you sure that your category is: \(category.title)", message: "\(category.title)", preferredStyle: UIAlertController.Style.actionSheet)
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.indicator.startAnimating()
            sSelf.navigationController?.navigationBar.topItem?.title = "Loading..."
            sSelf.mainLabel.text = "Loading..."
            UIView.transition(with: sSelf.alphaView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: { [weak self] in
                guard let sSelf = self else { return }
                sSelf.alphaView.isHidden = false
            })
            NetworkManager.shared.getQuestions(id: category.id) { questions in
                for question in questions {
                    RealmManager.shared.writeQuestions(question: SavedQuestions(questionID: question.questionID, questionCode: question.questionCode, question: question.question, comment: question.comment, categoryID: question.categoryID, origin: question.origin, categoryNewID: question.categoryNewID, isAnswered: false))
                    print("questionCode: \(question.questionCode)")
                }
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.indicator.stopAnimating()
                    UIView.transition(with: sSelf.alphaView, duration: 0.4,
                                      options: .transitionCrossDissolve,
                                      animations: { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.alphaView.isHidden = true
                    })
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
                    sSelf.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "No", style: .destructive, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            print(sSelf)
            print("Change category")
        }))
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            sSelf.present(alertController, animated: true, completion:{})
        }
        
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.convert(mainLabel.frame.origin, to: self.view).y <= 60 {
            navigationController?.navigationBar.topItem?.title = mainLabel.text
            mainLabel.font = mainLabel.font.withSize(1)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(categories[indexPath.row].id, forKey: "category_id")
        self.downdloadQuestions(category: categories[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CatrgoryCell.self), for: indexPath)
        guard let mainCell = cell as? CatrgoryCell else {return cell}
        mainCell.categoryLabel.text = categories[indexPath.row].title
        return mainCell
    }
    
    
}
