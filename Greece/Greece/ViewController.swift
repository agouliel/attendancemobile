//
//  ViewController.swift
//  Greece
//
//  Created by Ilya Rabyko on 15.02.22.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func sendAnswer(_ sender: Any) {
        let docParams: [String: Any] = [
            "answer": [
                "InspectorName": "Ilya",
                "answer": 1,
                "comment": "test",
                "questionid": "test",
                "questioncode": "test",
                "categoryid": 0,
                "origin": "test",
                "categorynewid": "test"
            ]
        ]
        
        AF.request("http://dev.eraappmobile.com/api/answer", method: .post, parameters: docParams, encoding: JSONEncoding.default).responseString { result in
            switch result.result {
            case .success(let value):
                print(result)
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
}

